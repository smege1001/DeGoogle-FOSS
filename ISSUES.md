# Issues

## Legend

- [x] [URL] = ***Merged***
- [ ] [URL] = ***Active***
- [ ] ~~[URL]~~ = ***Rejected***

## List of issues

1. [ ] https://github.com/django/djangoproject.com/issues/1003
2. [x] https://github.com/gnachman/iterm2-website/issues/76
3. [x] https://github.com/stumpwm/stumpwm/issues/793
4. [ ] https://github.com/neovim/neovim.github.io/issues/183
5. [ ] https://github.com/arcticicestudio/nord-docs/issues/195
6. [ ] https://github.com/stayradiated/terminal.sexy/issues/65
7. [ ] https://github.com/tholman/elevator.js/issues/131
8. [ ] https://github.com/madrobby/zeptojs.com/issues/5
9. [ ] https://github.com/kriskowal/q/issues/848
10. [ ] https://bugs.archlinux.org/task/61584
11. [ ] https://github.com/ourresearch/get-unsub/issues/5
12. [ ] https://github.com/dtao/lazy.js/issues/231
13. [ ] https://github.com/godotengine/godot-docs/issues/3792
14. [ ] https://github.com/ohmyzsh/ohmyzsh/issues/9113
15. [ ] https://github.com/erldocs/erldocs/issues/75
16. [ ] https://github.com/cdnjs/static-website/issues/16
17. [ ] https://github.com/neovim/neovim.github.io/issues/185
18. [ ] https://github.com/yeoman/yeoman/issues/1739
19. [ ] https://github.com/compiler-explorer/compiler-explorer/issues/2078
20. [ ] https://github.com/majodev/google-webfonts-helper/issues/114
21. [ ] https://github.com/vuejs/vuejs.org/issues/2641
22. [ ] https://github.com/d3/d3.github.com/pull/21
23. [ ] https://github.com/animate-css/animate.css/issues/1083
24. [ ] https://github.com/mrdoob/three.js/issues/19841
25. [ ] https://github.com/ReactiveX/rxjs/issues/5590
26. [ ] https://github.com/mui-org/material-ui/issues/21799
27. [ ] https://github.com/reduxjs/redux/issues/3815
28. [X] https://github.com/jquery/jquery.com/issues/208
29. [ ] https://github.com/reveal/revealjs.com/issues/5
30. [ ] https://github.com/socketio/socket.io/issues/3618
31. [ ] https://github.com/chartjs/Chart.js/issues/7621
32. [ ] https://github.com/Semantic-Org/Semantic-UI-Docs/issues/439
33. [ ] https://github.com/moment/momentjs.com/issues/665
34. [ ] https://github.com/jgthms/bulma/issues/3042
35. [ ] https://github.com/necolas/normalize.css/issues/823
36. [ ] https://github.com/Dogfalo/materialize/issues/6578
37. [ ] https://github.com/babel/website/issues/2292
38. [ ] https://github.com/juliangarnier/anime/issues/709
39. [ ] https://github.com/impress/impress.js/issues/770
40. [ ] https://github.com/ColorlibHQ/gentelella/issues/901
41. [ ] https://github.com/gulpjs/gulp/issues/2465
42. [ ] https://github.com/lubuntu-dev/lubuntu.net/issues/5
43. [ ] https://github.com/os-js/os-js.org/issues/12
44. [ ] ~~https://github.com/Homebrew/brew.sh/issues/510~~